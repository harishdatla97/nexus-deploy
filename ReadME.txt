# Overview

The Nexus Deployer project exists to deploy TSP artifacts into Nexus. It consists of a Python script and related files.

The deployer works by dynamically constructing a command line invocation of the Maven Deploy plugin, which allows for the deployment of arbitrary artifacts into a binary repository. The script is intended only for use with TSP files and will fail with any other file type. 

# Dependencies

Python 2.7 is required to run the deployer.

Only packages in the standard library are used, but the ```jsonschema``` library is required for schema validation.


# Environment

The Maven configuration assumes the presence of two environment variables:

* ```bamboo_NEXUS_USER```
* ```bamboo_NEXUS_PASSWORD```

The variables are named this way because when adding environment variables in Bamboo, the ```bamboo_``` prefix is added to each variable name, so that a variable named ```NEXUS_USER``` in the Bamboo interface becomes ```bamboo_NEXUS_USER``` in the job's environment. If running the deployer from the command line, ensure that the variables are defined as named above in the shell.

The user and password values must equal the Nexus user and password to be used when uploading artifacts.

# Artifact Definition File

In order to minimize the number of arguments passed into the deployer, the metadata which will be associated with each artifact is stored in a source controlled file named [artifacts.json](artifacts.json). 

The file has the following format:

Attribute | Value
--- | ---
id | the key by which the artifact is identified as an argument to the deployer
group | the Nexus group id value
artifact | the Nexus artifact id value
packaging | the artifact's file extension
classifier | an additional identifier used by the Auto Updates project to identify the artifact

A JSON schema file is stored in [artifacts-schema.json](artifacts-schema.json). To validate the artifacts definition file against the schema, run the [validate_artifacts.sh](validate_artifacts.sh) script.

# Development

For use in a local environment, use the ```--dev true``` argument to [deploy_tsp.py](deploy_tsp.py). This will ensure that the [settings-dev.xml](settings-dev.xml) file is used, which includes a proxy definition. This proxy definition assumes you have cntlm installed locally. Please see [Linux Configuration Notes](http://mlbconfluence:8090/display/KSB/Linux+Configuration+Notes) for further details.

The ```--dev``` flag assumes you have an instance of Nexus running on the local machine with the default admin user name and password. 

See the [test.sh](test.sh) script as an example of how to use the ```--dev``` flag.
