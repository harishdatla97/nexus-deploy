#!/usr/bin/python2

import json
import sys
import argparse
import os
import tarfile
import re


def deploy(artifact, path, is_dev):

    # determine development-related settings
    settings_file = 'settings-dev.xml' if is_dev else 'settings.xml'
    host = 'localhost' if is_dev else 'us08nexus01p'

    # open the TSP and extract the part number file
    # tar = tarfile.open(path)
    # base = os.path.basename(path)
    # root = os.path.splitext(base)[0]
    # swpn = tar.extractfile("{}.swpn".format(root))

    # read the version from the part number file
     version = '$(date +%d%m%y%M%S)'
    # ver1 = ''
    # for line in swpn.readlines():
        # if line.lower().startswith("swversion"):
            # ver1 = re.split('=', line)[1].rstrip()
            # version = ver1.strip()
            # print (version) 
            # break

     if version == '':
         raise Exception("Unable to locate SwVersion in TSP's swpn file.")

    # assemble the Maven command arguments
    args = {
        'url': "http://{}:8081/nexus/content/repositories/STAGING".format(host),
        'settings_file': settings_file,
        'group': artifact['group'],
        'artifact': artifact['artifact'],
        'packaging': artifact['packaging'],
        'classifier': artifact['classifier'],
        'version': version,
        'path': path
    }
    print (args) 

    # execute the Maven commnand
    cmd = "mvn deploy:deploy-file -Durl={url} " \
          "--settings {settings_file} " \
          "-DrepositoryId=nexus " \
          "-Dfile={path} " \
          "-DgroupId={group} " \
          "-DartifactId={artifact} " \
          "-Dversion={version} " \
          "-Dpackaging={packaging} " \
          "-DgeneratePom=true"
    cmd = cmd.format(**args)
    print (cmd) 

    # raise an exception if Maven returns an error code
    if os.WEXITSTATUS(os.system(cmd)) != 0:
        raise Exception("Maven command returned non-zero exit code")


def main(argv):
    parser = argparse.ArgumentParser(description='Deploy an artifact to Nexus\n ********************************************************', epilog="""All's well that ends well.""")
	
    parser.add_argument('-a, --artifact_id', dest='artifact_id', required=True,
                        help='The name of the artifact as defined in the artifacts manifest.')

    parser.add_argument('--path', dest='path', required=True,
                        help='Path to the TSP to be deployed.')

    parser.add_argument('--dev', dest='is_dev', required=False,
                        help='Set to true if using for development.')

    args = parser.parse_args(argv[1:])

    # read the artifacts descriptor as JSON
    with open('artifacts.json') as data_file:
        artifacts = json.load(data_file)

    artifact_definition_exists = False

    # find the artifact definition by id, then deploy
    for artifact in artifacts:
        if args.artifact_id == artifact["id"]:
            artifact_definition_exists = True
            deploy(artifact=artifact, path=args.path, is_dev=args.is_dev)
            break

    if not artifact_definition_exists:
        raise Exception("No artifact was found with identifier: " + args.artifact)


if __name__ == "__main__":
    main(sys.argv)
